#include <wiringPi.h>
#include <stdio.h>

#define WPI_PIN 3 /* GPIO-8 */

int main (void)
{
  wiringPiSetup();

  pinMode(WPI_PIN, OUTPUT);

  digitalWrite(WPI_PIN, HIGH); 

  return 0 ;
}

